package shaun.test.stateless.header;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author miemie
 * @since 2019-07-25
 */
@SpringBootApplication
public class HeaderApplication {

    public static void main(String[] args) {
        SpringApplication.run(HeaderApplication.class, args);
    }
}
